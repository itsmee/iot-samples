#! /usr/bin/python
from app import app
from flask import render_template, flash, redirect
from flask import Flask, Response, redirect, url_for, request, session, abort

########################
## mqtt
## https://github.com/stlehmann/Flask-MQTT/blob/master/doc/usage.rst
########################
import eventlet
import json
from flask import Flask, render_template
from flask_mqtt import Mqtt
from flask_socketio import SocketIO
from flask_bootstrap import Bootstrap

eventlet.sleep(1)
eventlet.monkey_patch()

########################
## MQTT 
########################
mqtt4mqttpage = Mqtt(app)
print app.config["MYSQL_DATABASE_HOST"]
mqtt = Mqtt(app)
socketio = SocketIO(app)
bootstrap = Bootstrap(app)


@socketio.on('publish')
def handle_publish(json_str):
    data = json.loads(json_str)
    mqtt4mqttpage.publish(data['topic'], data['message'])


@socketio.on('subscribe')
def handle_subscribe(json_str):
    mqtt.subscribe("sensor/temperature/wetter.com")
    mqtt.subscribe("sensor/temperature/ESP_13E684")

    data = json.loads(json_str)
    mqtt4mqttpage.subscribe(data['topic'])

@socketio.on('unsubscribe_all')
def handle_unsubscribe_all():
    mqtt4mqttpage.unsubscribe_all()

@mqtt4mqttpage.on_message()
def handle_mqtt_message(client, userdata, message):
    data = dict(
        topic=message.topic,
        payload=message.payload.decode()
    )
    socketio.emit('mqtt_message', data=data)

@mqtt.on_message()
def handle_mqtt_message(client, userdata, message):
    if message.topic == "sensor/temperature/wetter.com":
        socketio.emit('tempWetterCom', {'data': message.payload})
    if message.topic == "sensor/temperature/ESP_13E684":
        socketio.emit('tempGarten', {'data': message.payload})

########################
## Main
########################
if __name__ == '__main__':
  socketio.run(app, host='0.0.0.0', port=5000, use_reloader=True, debug=True)

