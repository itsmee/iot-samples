from flask import Flask
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_login import LoginManager, UserMixin, login_required

app = Flask(__name__)

login = LoginManager(app)
login.login_view = 'login'

########################
## Read Config
########################
app.config.from_envvar("CONFIGFILE")

########################
## Main
########################
db = SQLAlchemy(app)
db.init_app(app)
migrate = Migrate(app, db)

from app import routes
from app.classes import User

@login.request_loader
def load_user(request):
    token = request.headers.get('Authorization')
    if token is None:
        token = request.args.get('token')

    if token is not None:
        username,password = token.split(":") # naive token
        user_entry = User.get(username)
        if (user_entry is not None):
            user = User(user_entry[0],user_entry[1])
            if (user.password == password):
                return user
    return None

@login.user_loader
def load_user(id):
    return User.query.get(int(id))

## Create db tables - if necessary
db.create_all()
db.session.commit()
