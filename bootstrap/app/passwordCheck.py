#! /usr/bin/python
#
# https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-v-user-logins
#
# ./passwordCheck.py 'pbkdf2:sha256:50000$yUQwj5cK$41bb568456a9bef62e3785a9a4203040ffe64e3a58d616d8cadc78c4ec246ef6' 123

import sys

from werkzeug.security import check_password_hash

print check_password_hash(sys.argv[1], sys.argv[2]) # Hash, password
#print check_password_hash("pbkdf2:sha256:50000$yUQwj5cK$41bb568456a9bef62e3785a9a4203040ffe64e3a58d616d8cadc78c4ec246ef6", "123")
