from __future__ import print_function
from app import app
from app.forms import LoginForm
from flask import render_template, flash, redirect, request, jsonify
import pymysql
import json 
import sys
import re
from werkzeug.urls import url_parse
from datetime import datetime
from flask_login import login_user, logout_user, login_required, current_user
from app.classes import User


########################
## functions
########################
def querySql( sqlQuery )  :
    db = pymysql.connect(app.config['MYSQL_DATABASE_HOST'],app.config['MYSQL_DATABASE_USER'],app.config['MYSQL_DATABASE_PASSWORD'],app.config['MYSQL_DATABASE_DB'], charset='utf8')
    cursor = db.cursor()
    cursor.execute(sqlQuery)

    r = [dict((cursor.description[i][0], value)
              for i, value in enumerate(row)) for row in cursor.fetchall()]
    db.close()
    return r

def querySqlDump( sqlQuery )  :
    db = pymysql.connect(app.config['MYSQL_DATABASE_HOST'],app.config['MYSQL_DATABASE_USER'],app.config['MYSQL_DATABASE_PASSWORD'],app.config['MYSQL_DATABASE_DB'], charset='utf8')
    cursor = db.cursor()
    cursor.execute(sqlQuery)

    results = cursor.fetchall()
    return results

def getdata(sqlQuery):
    db = pymysql.connect(app.config['MYSQL_DATABASE_HOST'],app.config['MYSQL_DATABASE_USER'],app.config['MYSQL_DATABASE_PASSWORD'],app.config['MYSQL_DATABASE_DB'], charset='utf8')
    cursor = db.cursor()
    cursor.execute(sqlQuery)

    ones = [[i[0]*1000, i[1]] for i in cursor.fetchall()]
    return json.dumps(ones)

def update( SQL, VALUE, WHERE)  :
    db = pymysql.connect(app.config['MYSQL_DATABASE_HOST'],app.config['MYSQL_DATABASE_USER'],app.config['MYSQL_DATABASE_PASSWORD'],app.config['MYSQL_DATABASE_DB'], charset='utf8')
    cursor = db.cursor()

    print(SQL+ " "+VALUE+" "+ WHERE)
    cursor.execute(SQL, (VALUE, WHERE))
    print("Number of rows updated: "+str(cursor.rowcount))
    db.commit()

    db.close()



########################
## views
########################

#######################
## Dashboard - this really needs content..
#######################
columnsIotSensors = [
  { "field": "TS",        "title": "Timestamp", "sortable": True, "formatter":"timestampFormatter" },
  { "field": "SENSOR_TYPE",     "title": "Type",       "sortable": True, },
  { "field": "OBJECT",          "title": "Object",     "sortable": True, },
  { "field": "VALUE",           "title": "Value",      "sortable": True, },
  { "field": "UNIT",            "title": "Unit",       "sortable": True, },
  { "field": "KEY",             "title": "Key",        "sortable": True, "visible":False, }
]

@app.route('/index')
@app.route('/dashboard')
@app.route('/')
@login_required
def dashboard():
    data=querySql("SELECT * FROM `iot_sensors` order by TS desc limit 100")
    return render_template("dashboard.html", table1=data, table1name="Recent DB Events", columnsT1=columnsIotSensors)

#######################
## oel
#######################
columnsOel= [
    { "field": "DATE",      "title":"Date",        "sortable": True, "formatter":"dateFormatter", "visible":True },
    { "field": "LITER",     "title":"Fuellstand (l)","sortable": True, "visible":True },
    { "field": "VERBRAUCH", "title":"Verbrauch (l/d)","sortable": True, "visible":True },
            ]

@app.route('/oel')
@login_required
def oel():
  oel=querySql("SELECT * FROM `iot_oel` order by DATE desc limit 1000")

#  alarm1=getdata("select UNIX_TIMESTAMP(TS) as timestamp, cast(VALUE as int) from iot_sensors where OBJECT='WIN-1' and SENSOR_TYPE='ALARM' and TS > NOW() - INTERVAL 3 day ORDER BY `iot_sensors`.`TS` ASC, iot_sensors.VALUE")
  oelVorrat=getdata("SELECT UNIX_TIMESTAMP(date) as timestamp, cast(liter as int) FROM iot_oel order by DATE desc limit 1000")
  #oelVerbrauch=getdata("SELECT UNIX_TIMESTAMP(date) as timestamp, cast(verbrauch as int)  FROM iot_oel order by DATE desc limit 1000")
  oelVerbrauch=getdata("SELECT UNIX_TIMESTAMP(oel.DATE), cast(oel.verbrauch as int) as verbrauch FROM iot_oel oel inner join iot_temp temp on oel.date=date(temp.ts) where oel.date=date(temp.ts) group by oel.date")

  #tempWetterCom=getdata("select UNIX_TIMESTAMP(TS) as timestamp, AVG_VALUE as meassure from iot_temp where OBJECT='wetter.com' order by TS ASC , OBJECT ASC limit 1000")
  tempWetterCom=getdata("SELECT UNIX_TIMESTAMP(oel.DATE) as date, cast(temp.min_value as int) as min_temp FROM iot_oel oel inner join iot_temp temp on oel.date=date(temp.ts) where oel.date=date(temp.ts) group by oel.date")

  return render_template("oel.html",table1=oel, table1name="Fuellstand Heiztank", columnsT1=columnsOel, data1=oelVorrat, data1name="Fuellstand (l)", data2=oelVerbrauch, data2name="Verbrauch (l/d)", data3=tempWetterCom, data3name="MinTemp")


#######################
## artirain
#######################
columnsWater= [
        { "field": "DATE",      "title":"Date",        "sortable": True, "formatter":"dateFormatter", "visible":True },
        { "field": "LITER",     "title":"Verbrauch(l)","sortable": True, "visible":True },
]

columnsRain= [
        { "field": "DATE",      "title":"Date",        "sortable": True, "formatter":"dateFormatter", "visible":True },
        { "field": "MM_PREDICT","title":"Prognose(mm)","sortable": False, "visible":True },
        { "field": "MM_ACTUAL", "title":"Ist(mm)",     "sortable": True,  "visible":True },
        { "field": "DEG_MAX",   "title":"Max",  "sortable": False, "visible":True },
        { "field": "DEG_MIN",   "title":"Min",  "sortable": False, "visible":True },
#        { "field": "DEG_AVG ",  "title":"Avg",  "sortable": False, "visible":False },
        { "field": "ART_LITER_TOTAL",  "title":"Beregnet (l)","sortable": False, "formatter":"boolFormatter", "visible":True},
#        { "field": "IS_EXECUTED",  "title":"schon beregnet?","sortable": False, "formatter":"boolFormatter", "visible":True},
#        { "field": "IS_MANUAL",  "title":"manuell","sortable": False, "formatter":"boolFormatter", "visible":True},
#        { "field": "ART_EVENTS",  "title":"Events",    "sortable": False, "visible":True},
]

columnsPropose= [
        { "field": "DATE",      "title":"Date",        "sortable": True, "formatter":"dateFormatter", "visible":True },
        { "field": "MM_PREDICT","title":"Prognose(mm)","sortable": False, "visible":True },
        { "field": "DEG_MAX",   "title":"Max",  "sortable": False, "visible":True },
        { "field": "DEG_MIN",   "title":"Min",  "sortable": False, "visible":True },
        { "field": "ART_MIN_R1",  "title":"R1 (min)","sortable": False, "visible":True, "editable":True},
        { "field": "ART_MIN_R2",  "title":"R2 (min)","sortable": False, "visible":True, "editable":True},
        { "field": "ART_MIN_R3",  "title":"R3 (min)","sortable": False, "visible":True, "editable":True},
        { "field": "ART_MIN_R4",  "title":"R4 (min)","sortable": False, "visible":True, "editable":True},
        { "field": "ART_MIN_R5",  "title":"R5 (min)","sortable": False, "visible":True, "editable":True},
        { "field": "ART_MIN_R6",  "title":"R6 (min)","sortable": False, "visible":True, "editable":"True"},
        { "field": "IS_EXECUTED",  "title":"schon beregnet?","sortable": False, "formatter":"boolFormatter", "visible":True},
        { "field": "IS_MANUAL",  "title":"manuell","sortable": False, "formatter":"boolFormatter", "visible":True},
        { "field": "ART_EVENTS",  "title":"Events",    "sortable": False, "visible":True},
] 

columnsProposeHist= [
        { "field": "DATE",      "title":"Date",        "sortable": True, "formatter":"dateFormatter", "visible":True },
        { "field": "MM_PREDICT","title":"Prognose(mm)","sortable": False, "visible":True },
        { "field": "DEG_MAX",   "title":"Max",  "sortable": False, "visible":True },
        { "field": "DEG_MIN",   "title":"Min",  "sortable": False, "visible":True },
        { "field": "ART_MIN_R1",  "title":"R1 (min)","sortable": False, "visible":True},
        { "field": "ART_MIN_R2",  "title":"R2 (min)","sortable": False, "visible":True},
        { "field": "ART_MIN_R3",  "title":"R3 (min)","sortable": False, "visible":True},
        { "field": "ART_MIN_R4",  "title":"R4 (min)","sortable": False, "visible":True},
        { "field": "ART_MIN_R5",  "title":"R5 (min)","sortable": False, "visible":True},
        { "field": "ART_MIN_R6",  "title":"R6 (min)","sortable": False, "visible":True},
        { "field": "ART_LITER_R1",  "title":"R1 (l)","sortable": False, "visible":True},
        { "field": "ART_LITER_R2",  "title":"R2 (l)","sortable": False, "visible":True},
        { "field": "ART_LITER_R3",  "title":"R3 (l)","sortable": False, "visible":True},
        { "field": "ART_LITER_R4",  "title":"R4 (l)","sortable": False, "visible":True},
        { "field": "ART_LITER_R5",  "title":"R5 (l)","sortable": False, "visible":True},
        { "field": "ART_LITER_R6",  "title":"R6 (l)","sortable": False, "visible":True},
        { "field": "IS_EXECUTED",  "title":"schon beregnet?","sortable": False, "formatter":"boolFormatter", "visible":True},
        { "field": "IS_MANUAL",  "title":"manuell","sortable": False, "formatter":"boolFormatter", "visible":True},
        { "field": "ART_EVENTS",  "title":"Events",    "sortable": False, "visible":True},
] 

@app.route('/rain')
@app.route('/artirain')
@login_required
def artirain():
    rain=querySql("SELECT * FROM `rain` order by DATE desc limit 14")
    waterPerDay=querySql("SELECT * FROM `iot_water` order by DATE desc limit 14")
    proposal=querySql("select * from IOT.rain where DATE=CURDATE()")
    proposalHist=querySql("select * from IOT.rain order by DATE desc limit 10 ")
    print(proposal)
    return render_template("artirain.html",table1=rain, table1name="Regen der letzten Tage", table2=waterPerDay, table2name="Verbrauch der letzten Tage", columnsT1=columnsRain, columnsT2=columnsWater, table3=proposal, table3name="Bewaesserung - Vorschlag (Editierbar)", columnsT3=columnsPropose, table4=proposalHist, columnsT4=columnsProposeHist, table4name="Bewaesserung - History")

@app.route('/rainStoreResult', methods=['GET', 'POST'])
@login_required
def artirainStoreResult():
  if request.method == 'POST':
    print("POST")
    multi_dict = request.args
    date=multi_dict.get("DATE")
    value=multi_dict.get("VALUE")
    key=multi_dict.get("KEY")

#    for key in multi_dict:
#      print(key+":"+multi_dict.get(key))
#      print(multi_dict.getlist(key))

    print("update IOT.rain set "+key+"='"+value+"' where DATE='"+date+"'")
    print("update IOT.rain set "+key+"=%s where DATE=%s", value, date)
    update("update IOT.rain set "+key+"=%s where DATE=%s", value, date)

    return value
  else:
    print("ERROR - expected POST")

  return "0"


######################
## MQTT
######################
@app.route('/mqtt')
@login_required
def mqtt():
    return render_template("mqtt.html")

@app.route('/adminlte')
@login_required
def adminlte():
    return render_template("adminlte.html")

@app.route('/newframework')
@login_required
def newframework():
    return render_template("newframework.html")

#######################
## login
#######################
@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect('/')
    form = LoginForm()
    print ("1")
    if form.validate_on_submit():
      print ("2")
      user = User.query.filter_by(username=form.username.data).first()
      if user is None or not user.check_password(form.password.data):
        print ("3")
	flash('Invalid username or password')
	return redirect('/login')
      login_user(user, remember=form.remember_me.data)
      print ("3")
      next_page = '/artirain'
      if not next_page or url_parse(next_page).netloc != '':
        next_page = url_for('index')
      return redirect(next_page)
    print ("4")
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect('/')

#######################
## Motion & Window
#######################
columnsAlarm= [
        { "field": "TS",        "title":"Timestamp",   "sortable": True, "formatter":"timestampFormatter", "visible":True },
        { "field": "OBJECT",    "title":"Device",      "sortable": True, "visible":True },
        { "field": "VALUE",     "title":"Anwesend",    "sortable": False, "visible":True },
]

@app.route('/alarm')
@login_required
def alarm(chartID = 'chart_ID', chart_type = 'spline', chart_height = 350):
    alarm=querySql("select * from iot_sensors where (SENSOR_TYPE='ALARM') and TS > NOW() - INTERVAL 3 day ORDER BY `iot_sensors`.`TS` DESC, iot_sensors.VALUE")
    alarm1=getdata("select UNIX_TIMESTAMP(TS) as timestamp, cast(VALUE as int) from iot_sensors where OBJECT='WIN-1' and SENSOR_TYPE='ALARM' and TS > NOW() - INTERVAL 3 day ORDER BY `iot_sensors`.`TS` ASC, iot_sensors.VALUE")
    alarm2=getdata("select UNIX_TIMESTAMP(TS) as timestamp, cast(VALUE as int) from iot_sensors where OBJECT='CT60-1' and SENSOR_TYPE='ALARM' and TS > NOW() - INTERVAL 3 day ORDER BY `iot_sensors`.`TS` ASC, iot_sensors.VALUE")
#    presenceSandra=getdata("select UNIX_TIMESTAMP(TS) as timestamp, cast(VALUE as int) from iot_sensors where OBJECT='mobile-sandra' and SENSOR_TYPE='Presence' and TS > NOW() - INTERVAL 1 week ORDER BY `iot_sensors`.`TS` ASC")
#    presenceHerbert=getdata("select UNIX_TIMESTAMP(TS) as timestamp, cast(VALUE as int) from iot_sensors where OBJECT='mobile-herbert' and SENSOR_TYPE='Presence' and TS > NOW() - INTERVAL 1 week ORDER BY `iot_sensors`.`TS` ASC")
    return render_template("alarm.html", table1=alarm, table1name="Alarmmeldungen", columnsT1=columnsPresence, data1=alarm1, data1name="WIN-1", data2=alarm2, data2name="CT60-1" )



#######################
## presence
#######################
columnsPresence= [
        { "field": "TS",        "title":"Timestamp",   "sortable": True, "formatter":"timestampFormatter", "visible":True },
        { "field": "OBJECT",    "title":"Device",      "sortable": True, "visible":True },
        { "field": "VALUE",     "title":"Anwesend",    "sortable": False, "visible":True },
]

@app.route('/presence')
@login_required
def presence(chartID = 'chart_ID', chart_type = 'spline', chart_height = 350):
    presence=querySql("select * from iot_sensors where SENSOR_TYPE='Presence' and TS > NOW() - INTERVAL 1 week ORDER BY `iot_sensors`.`TS` DESC")
    presenceAlex=getdata("select UNIX_TIMESTAMP(TS) as timestamp, cast(VALUE as int) from iot_sensors where OBJECT='mobile-alex' and SENSOR_TYPE='Presence' and TS > NOW() - INTERVAL 1 week ORDER BY `iot_sensors`.`TS` ASC")
    presenceSandra=getdata("select UNIX_TIMESTAMP(TS) as timestamp, cast(VALUE as int) from iot_sensors where OBJECT='mobile-sandra' and SENSOR_TYPE='Presence' and TS > NOW() - INTERVAL 1 week ORDER BY `iot_sensors`.`TS` ASC")
    presenceHerbert=getdata("select UNIX_TIMESTAMP(TS) as timestamp, cast(VALUE as int) from iot_sensors where OBJECT='mobile-herbert' and SENSOR_TYPE='Presence' and TS > NOW() - INTERVAL 1 week ORDER BY `iot_sensors`.`TS` ASC")
    return render_template("presence.html", table1=presence, table1name="An-/Abwesenheit", columnsT1=columnsPresence , data1=presenceAlex, data1name="Alex", data2=presenceSandra, data2name="Sandra", data3=presenceHerbert, data3name="Herbert" )


#######################
## temperature
#######################
@app.route('/temperature')
@login_required
def temperature(chartID = 'chart_ID', chart_type = 'spline', chart_height = 350):
    dataGarten=getdata("select UNIX_TIMESTAMP(TS) as timestamp, AVG_VALUE as meassure from iot_temp where OBJECT='Garten' order by TS ASC , OBJECT ASC limit 3000")
    dataAbstell=getdata("select UNIX_TIMESTAMP(TS) as timestamp, AVG_VALUE as meassure from iot_temp where OBJECT='Abstellkammer_OG' order by TS ASC , OBJECT ASC limit 3000")
    dataWetter=getdata("select UNIX_TIMESTAMP(TS) as timestamp, AVG_VALUE as meassure from iot_temp where OBJECT='wetter.com' order by TS ASC , OBJECT ASC limit 3000")

    tempWetterCom=querySql("select value from iot_sensors where SENSOR_TYPE='TEMP' and OBJECT='wetter.com' ORDER BY TS DESC LIMIT 1")
    tempWetterCom=tempWetterCom[0]
    tempWetterCom=tempWetterCom['value']

    tempGarten=querySql("select value from iot_sensors where SENSOR_TYPE='TEMP' and OBJECT='Garten' ORDER BY TS DESC LIMIT 1")
    tempGarten=tempGarten[0]
    tempGarten=tempGarten['value']
    
    return render_template('temperature.html', tempWetterCom=tempWetterCom, tempGarten=tempGarten, data1=dataGarten, data1name='Garten', data2=dataAbstell, data2name='Abstellkammer', data3=dataWetter, data3name='Wetter.com')


#######################
## hifi
#######################
@app.route('/hifi')
@login_required
def hifi():
    return render_template('hifi.html')


#######################
## buttons
#######################
@app.route('/buttons')
@login_required
def buttons():
    return render_template('buttons.html')



#######################
## links
#######################
@app.route('/links')
@login_required
def links():
    return render_template('links.html')


