function dateFormatter(value) {
  var date = new Date(value)
  return pad(date.getDate(),2)+"."+pad(date.getMonth()+1,2)+"."+date.getFullYear()
}; 
function timestampFormatter(value) {
  var date = new Date(value)
  return pad(date.getDay(),2)+"."+pad(date.getMonth()+1,2)+"."+date.getFullYear()+" "+pad(date.getUTCHours(),2)+":"+pad(date.getMinutes(),2)
};
function boolFormatter(value) {
  if ( value == "1") {
    return "Yes" 
  } else if ( value == "0") {
    return "-"
  } else {
    return "N/A"
  }
};
function pad(num, size) {
  var s = num+"";
  while (s.length < size) s = "0" + s;
  return s;
}

