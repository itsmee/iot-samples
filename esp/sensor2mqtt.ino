//*******************************************************************
// ESP8266 / ESP32 sketch that'll read from sensors (e.g. temp from DS18)
// and deliver results to an MQTT queue
//*******************************************************************
const String VERSION = "S4-send2mqtt_v6.ino 2018-04-02";
#include "config_sample.h"

//*******************************************************************
// INCLUDE ESP CAPABILITIES DEFINITION - what are we capable of doing?
//*******************************************************************
String CAPABILITIES="TEMP:"+String(has_temp_sensor)
                  +" KEEPALIVE:"+String(has_callhome)
                  +" MOTION:"+String(has_motion_sensor)
                  +" WATER:"+String(has_water_sensor)
                  +" TASTER:"+String(has_taster)
                  +" ULTRASONIC:"+String(has_ultrasonic_sensor)
                  +" VIBRATION:"+String(has_vibration_sensor)
                  +" DHT11:"+String(has_dht11_sensor)
                  +" SONOFF_RELAIS:"+String(has_sonoff_relais)
                  +" LED:"+String(has_led);

//*******************************************************************
// be sure correct IDE and settings are used for ESP8266 or ESP32
//*******************************************************************
#if !(defined( ARDUINO_ARCH_ESP8266) || defined(ARDUINO_ARCH_ESP32))
#error Oops!  Make sure you have 'ESP8266 or ESP32' compatible board selected from the 'Tools -> Boards' menu.
#endif


//******************************************************************
// Includes: ESP32 vs ESP8266
//*******************************************************************
#if (defined( ARDUINO_ARCH_ESP8266))
  #include <ESP8266WiFi.h>
# endif
#if (defined( ARDUINO_ARCH_ESP32))
  #include <WiFi.h>
# endif

//*******************************************************************
// Includes: standard
//*******************************************************************
#include <PubSubClient.h> // MQTT (open the Arduino IDE, in the menu click on sketch -> include library -> manage libraries and install "PubSubClient by Nick O Leary"
#include <ArduinoJson.h>

#include <TimeLib.h>
#include <NtpClientLib.h>

#include <DallasTemperature.h>  // DS18 Temperature sensor
#include <ArduinoOTA.h>
#include <SimpleDHT.h>

#if ( has_pn532 == 1 )
  #include <SPI.h>                // PN 532
  #include <PN532_SPI.h> 
  #include <PN532.h>
#endif

//*******************************************************************
// getESPid - helper function to generate uniq msgs
//*******************************************************************
String macToID(const uint8_t* mac) {
  String result;
  for (int i = 3; i < 6; ++i) {
    result += String(mac[i], 16);
  }
  result.toUpperCase();
  return result;
}

String getESPid(void) {
  String id, espid;
  uint8_t mac[6];

  WiFi.macAddress(mac);
  id = macToID(mac);
  espid = "ESP_" + id;

  return espid; 
}

//*******************************************************************
// Variables
// ensure pubsub.h has MQTT_MAX_PACKET_SIZE=1024
//*******************************************************************

String hw_info_topic     = String("info/") + getESPid();
String temperature_topic = String("sensor/") + String("temperature/") + getESPid();
String motion_topic      = String("sensor/") + String("motion/") + getESPid();
String flowcontrol_topic = String("sensor/") + String("waterflow/") + getESPid();
String ultrasonic_topic  = String("sensor/") + String("oel/") + getESPid();
String callhome_topic    = String("sensor/") + String("callhome/") + getESPid();
String vibration_topic   = String("sensor/") + String("vibration/") + getESPid();
String humidity_topic    = String("sensor/") + String("humidity/") + getESPid();
String power_topic       = String("sensor/") + String("power/") + getESPid();
String action_topic      = String("action/") + OBJECT;

// different capabilities require different loops - there vars are used for them
unsigned long tempMillisPrev = 0;
unsigned long dhtMillisPrev = 0;
unsigned long mqMillisPrev = 0;
unsigned long ultrasonicMillisPrev = -3600000;  // Make sure it runs when powered on
unsigned long callhomeMillisPrev = 0;

// LED
#define BRIGHT    350     //max led intensity (1-500)
#define INHALE    1250    //Inhalation time in milliseconds.
#define PULSE     INHALE*1000/BRIGHT
#define REST      1000    //Rest Between Inhalations.

// dht11
SimpleDHT11 dht11;

// Taster
// ESP-01 GPIO Mapping   [0]=3,[2]=4,[4]=2,[5]=1,[12]=6,[13]=7,[14]=5,[15]=8,[16]=0
int tasterStatus = 0;
int tasterStatusLast = 0;

// SONOFF
int sonoffRelaisState=0;

// flow meter
volatile int   flow_frequency;     // Measures flow meter pulses
unsigned int   l_per_hour;         // Calculated litres/hour                      
float          l_per_interval;     // Calculated litres/interval
unsigned char  flowmeter = 4;      // Flow Meter Pin number
unsigned long  flowMillisPrev = 0;

// PIR
int pirStatus = 0;
int pirStatusLast = 0;

// wifi
int rssi=-99;
WiFiClient wifiClient;
PubSubClient mqttClient(wifiClient);

//oneWire
OneWire ds(pinDS18);  // a 4.7K resistor is necessary
DallasTemperature temp(&ds);

//*******************************************************************
// MQTT reconnect
//*******************************************************************
void mqtt_reconnect() {
  // Loop until we're reconnected
  while (!mqttClient.connected()) {
    //Serial.println("Attempting MQTT connection...");
    // Attempt to connect
    if (mqttClient.connect(getESPid().c_str(), mqtt_user, mqtt_password)) {
      Serial.println("connected");      
    } else {
      Serial.print("failed, rc=");
      Serial.print(mqttClient.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(1000);
    }
  }
  if(mqtt_subscribe_topic != "" ){
    mqttClient.subscribe(mqtt_subscribe_topic);
  }
}

//*******************************************************************
// MQTT callback for subscriptions
//*******************************************************************
void mqtt_callback(char* topic, byte* payload, unsigned int length) {
  StaticJsonBuffer<80> jsonBuffer;

  String payloadStr="";
  for (int i = 0; i < length; i++) {
    payloadStr+=(char)payload[i];
  }

  Serial.print("Message arrived [" + String(topic)+ " : " + payloadStr + "]") ;
  
  //*******************************************************************
  // SONOFF TH16 - set on / off
  //*******************************************************************
  if (has_sonoff_relais == 1) {
    JsonObject& root = jsonBuffer.parseObject(payload);
    if (!root.success()) {
      if ( payloadStr == "1" ) {
        sonoffRelaisState=1;
      } else if ( payloadStr == "0" ) {
        sonoffRelaisState=0;
      } else {
        Serial.println("parseObject(): "+ String(topic) + " : " + payloadStr + " failed");
       return;
      } 
    } else if ((String(topic)==mqtt_subscribe_topic)) {
      // {"cmd": {"relais": "0"}}
      sonoffRelaisState = root["cmd"]["relais"];
      if ( payloadStr == "1" ) {
        ledOn(pinLED);
      } else if ( payloadStr == "0" ) {
        ledOff(pinLED);
      } 
    }
 
    digitalWrite(pinRelais, sonoffRelaisState);
 
    String msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR_TYPE\":\"SONOFF-BUTTON\", \"OBJECT\":\""+OBJECT+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"POW-STATE-CHANGE\", \"VALUE\":\""+sonoffRelaisState+"\"}");
    Serial.println("POW-STATE-CHANGE: "+msg);
    mqttClient.publish(power_topic.c_str(), msg.c_str(), true);
  }
}

//*******************************************************************
// Time Helper: Print leading zero if < 10
//*******************************************************************
String printDigit(int digit) {
 String digitString="";
   if (digit < 10)
   digitString=('0');
 digitString+=digit;
 return digitString;
}

String getTimestamp() {
  String TS=year()+String("-")+printDigit(month())+("-")+printDigit(day()); 
  TS+=String(" ")+printDigit(hour())+":"+printDigit(minute())+":"+printDigit(second()); 
  return TS;
}

//*******************************************************************
// flow sensor
//*******************************************************************
void flow ()                  // Interrupt function
{ 
   flow_frequency++;
} 

//*******************************************************************
// vibration sensor
//*******************************************************************
long vib_init(){
  delay(10);
  long measurement=pulseIn (pinVibr, HIGH);  //wait for the pin to get HIGH and returns measurement
  return measurement;
}

//*******************************************************************
// Onboard LED
//*******************************************************************
void ledOn(int pin) {
  digitalWrite(pin , LOW);         // turn the LED on.
}

void ledOff(int pin) {
  digitalWrite(pin , HIGH);         // turn the LED off.
}

void ledGlow(int pin ) {
  for (int i=1;i<BRIGHT;i++){
      digitalWrite(pin, LOW);          // turn the LED on.
      delayMicroseconds(i*10);         // wait
      digitalWrite(pin, HIGH);         // turn the LED off.
    delayMicroseconds(PULSE-i*10);   // wait
    delay(0);                        //to prevent watchdog firing.
  }
}

//******************************************************
// Sensor Functions (HC-SR04 or HY-SRF05)
//******************************************************
float readUltrasonicDistance() {
  digitalWrite(pinUltrasonicTrig, HIGH);
  delayMicroseconds(10);
  digitalWrite(pinUltrasonicTrig, LOW);
  long duration = (pulseIn(pinUltrasonicEcho, HIGH)); 
  float distance = float(duration)/29/2;
  Serial.print("readUltrasonicDistance:");
  Serial.println(distance);
  return distance; 
}

//****************************************************
// Wifi
//****************************************************
void connectWifi(){
  int wifiConnectAttempts=0;
  while (WiFi.status() != WL_CONNECTED) {
    wifiConnectAttempts++;

    Serial.print("Connecting to "); Serial.println(ssid1);
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid1, password1);
    delay(15000); // I give it 15 sec to connect, if it fails, try next
  
    if (WiFi.status() != WL_CONNECTED) {
      Serial.print("Connecting to "); Serial.println(ssid2);
      WiFi.begin(ssid2, password2);
      delay(15000); // I give it 15 sec to connect, if it fails.....
    }
    if (WiFi.status() != WL_CONNECTED) {
      Serial.print("Connecting to "); Serial.println(ssid3);
      WiFi.begin(ssid3, password3);
      delay(15000); // I give it 15 sec to connect, if it fails.....
    }

    if((WiFi.status() != WL_CONNECTED) and (wifiConnectAttempts > 10 )) {
      Serial.print("Not connected - restarting");
      ESP.restart();
    }
  }

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
  rssi = 2 * (WiFi.RSSI() + 100);   // read RSSI and convert dbm to %
  Serial.println("RSSI (%): " + String(rssi));
}

//*******************************************************************
// Setup
//*******************************************************************
void setup(void) {
  Serial.begin(115200);

  //****************************************************
  // Version
  //****************************************************
  Serial.println(VERSION);

  //****************************************************
  // Wifi
  //****************************************************
  connectWifi();


  //****************************************************
  // NTP
  //****************************************************
  int8_t timeZone = 1;    // CET
  NTP.begin("pool.ntp.org",  timeZone, true);  
  NTP.setInterval(60000);
  delay(1000);

  //****************************************************
  // LED
  //****************************************************
  if ( has_led==1 ) { 
    pinMode(pinLED, OUTPUT);   // LED pin as output.    
    ledOn(pinLED);
  }

  //****************************************************
  // SONOFF Relais
  //****************************************************
  if ( has_sonoff_relais == 1 ) { 
    pinMode(pinRelais, OUTPUT);  
    //ledOn(pinLED_BLUE);
  }

  //****************************************************
  // MQTT - setup
  //****************************************************
  mqttClient.setServer(mqtt_server, mqtt_port);
  mqttClient.setCallback(mqtt_callback);

  // Subscribe to topic (maybe)
  if ( mqtt_subscribe_topic != "" ) 
    mqttClient.subscribe(mqtt_subscribe_topic);
  
  if (!mqttClient.connected()) {
    mqtt_reconnect();   // includes subscription
  }
  mqttClient.loop();
  
  IPAddress localAddr = WiFi.localIP();
  byte oct1 = localAddr[0];
  byte oct2 = localAddr[1];
  byte oct3 = localAddr[2];
  byte oct4 = localAddr[3];
  char s[16];
  
  sprintf(s, "%d.%d.%d.%d", oct1, oct2, oct3, oct4);
  String ip=s;

  pinMode(pinTaster, INPUT_PULLUP);

  temp.begin();
  
  //****************************************************
  // INFO - send on startup
  //****************************************************
  String msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"IP\", \"VALUE\":\""+ip+"\",\"UNIT\":\"N/A\",\"SENSOR_TYPE\":\"INFO\", \"OBJECT\":\""+OBJECT+"\"}");
  Serial.println("Info: "+msg);
  mqttClient.publish((hw_info_topic+"/IP").c_str(), msg.c_str(), true);
  
  msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"VERSION\", \"VALUE\":\""+VERSION+"\",\"UNIT\":\"N/A\",\"SENSOR_TYPE\":\"INFO\", \"OBJECT\":\""+OBJECT+"\"}");
  Serial.println("Info: "+msg);
  mqttClient.publish((hw_info_topic+"/VERSION").c_str(), msg.c_str(), true);

  msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"CAPABILITIES\", \"VALUE\":\""+CAPABILITIES+"\",\"UNIT\":\"N/A\",\"SENSOR_TYPE\":\"INFO\", \"OBJECT\":\""+OBJECT+"\"}");
  Serial.println("Info: "+msg);
  mqttClient.publish((hw_info_topic+"/CAPABILITIES").c_str(), msg.c_str(), true);

  msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"WLAN_STRENGTH\", \"VALUE\":\""+String(rssi)+"\",\"UNIT\":\"N/A\",\"SENSOR_TYPE\":\"INFO\", \"OBJECT\":\""+OBJECT+"\"}");
  Serial.println("Info: "+msg);
  mqttClient.publish((hw_info_topic+"/RECEPTION").c_str(), msg.c_str(), true);

  // ultrasonic
  if (has_ultrasonic_sensor == 1) {
    pinMode(pinUltrasonicTrig, OUTPUT);
    pinMode(pinUltrasonicEcho, INPUT);
  }
  
   // flow sensor
   pinMode(flowmeter, INPUT);
   attachInterrupt(flowmeter, flow, RISING); // Setup Interrupt 
                                     // see http://arduino.cc/en/Reference/attachInterrupt
   sei();                            // Enable interrupts  

  //****************************************************
  // OTA update - setup
  //****************************************************
  // Port defaults to 8266
  // ArduinoOTA.setPort(8266);

  // Hostname defaults to esp8266-[ChipID]
  ArduinoOTA.setHostname(OBJECT);

  // No authentication by default
  ArduinoOTA.setPassword("Set4OTA");

  // Password can be set with it's md5 value as well
  // MD5(admin) = 21232f297a57a5a743894a0e4a801fc3
  // ArduinoOTA.setPasswordHash("21232f297a57a5a743894a0e4a801fc3");

  ArduinoOTA.onStart([]() {
    String type;
    if (ArduinoOTA.getCommand() == U_FLASH)
      type = "sketch";
    else // U_SPIFFS
      type = "filesystem";

    // NOTE: if updating SPIFFS this would be the place to unmount SPIFFS using SPIFFS.end()
    Serial.println("Start updating " + type);
  });
  ArduinoOTA.onEnd([]() {
    Serial.println("\nEnd");
  });
  ArduinoOTA.onProgress([](unsigned int progress, unsigned int total) {
    Serial.printf("Progress: %u%%\r", (progress / (total / 100)));
  });
  ArduinoOTA.onError([](ota_error_t error) {
    Serial.printf("Error[%u]: ", error);
    if (error == OTA_AUTH_ERROR) Serial.println("Auth Failed");
    else if (error == OTA_BEGIN_ERROR) Serial.println("Begin Failed");
    else if (error == OTA_CONNECT_ERROR) Serial.println("Connect Failed");
    else if (error == OTA_RECEIVE_ERROR) Serial.println("Receive Failed");
    else if (error == OTA_END_ERROR) Serial.println("End Failed");
  });
  ArduinoOTA.begin();
  Serial.println("Ready for OTA");
  Serial.print("IP address: ");
  Serial.println(WiFi.localIP());
}

//****************************************************
// Loop
//****************************************************
void loop(void) { 
  //****************************************************
  // MQTT processing every 100ms
  //****************************************************
  if (millis() - mqMillisPrev > 100) {
    mqMillisPrev = millis();
    // Ensure that MQTT stays online, needed for subscription
    if (!mqttClient.connected()) {
      mqtt_reconnect();
    }
    mqttClient.loop();
    yield();
  }

  //****************************************************
  // Flow Sensor processing every 30 sec
  //****************************************************
  if ((has_water_sensor == 1) &&  (millis() - flowMillisPrev > 30000)) {
    flowMillisPrev = millis();
    if (flowMillisPrev !=0){	// skip metering during startup
      // Pulse frequency (Hz) = 7.5Q, Q is flow rate in L/min. (Results in +/- 3% range)
      l_per_hour     = (flow_frequency * 60 / 7.5 / 30 ); // (Pulse frequency x 60 min) / 7.5Q = flow rate in L/hour / 30 sec interval
      l_per_interval = (float(l_per_hour) * 30 / 60 / 60 ); // Water per 30sec interval
      flow_frequency = 0;                       // Reset Counter

      
      String msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR_TYPE\":\"WATER\", \"OBJECT\":\""+OBJECT+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"Gartenwasser\",\"VALUE\":\""+String(l_per_hour)+"\",\"UNIT\":\"l/h\"}");
      Serial.println("Gartenwasser: "+msg);
      mqttClient.publish(flowcontrol_topic.c_str(), msg.c_str(), true);
      
      msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR_TYPE\":\"WATER\", \"OBJECT\":\""+OBJECT+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"Gartenwasser\",\"VALUE\":\""+String(l_per_interval)+"\",\"UNIT\":\"l/int\"}");
      Serial.println("Gartenwasser: "+msg);
      mqttClient.publish(flowcontrol_topic.c_str(), msg.c_str(), true);
    }
    yield();
  }

  //****************************************************
  // Ultrasonic - every 3600sec / every hour
  //****************************************************
  if ((has_ultrasonic_sensor == 1) && (millis() - ultrasonicMillisPrev > 3600000)) {
    ultrasonicMillisPrev = millis();
    
    float ultrasonic_distance=readUltrasonicDistance();
    float oeltank_inhalt=int((143.4-ultrasonic_distance)*410*200/1000);
    oeltank_inhalt=round(oeltank_inhalt/10)*10;
    
    String msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR_TYPE\":\"ULTRASONIC\", \"OBJECT\":\""+OBJECT+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"CM_FREI\",\"VALUE\":\""+String(ultrasonic_distance)+"\",\"UNIT\":\"cm\"}");
    Serial.println("Ultrasonic: "+msg);
    mqttClient.publish((ultrasonic_topic+"/abstand").c_str(), msg.c_str(), true);
      
    msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR_TYPE\":\"ULTRASONIC\", \"OBJECT\":\""+OBJECT+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"INHALT\",\"VALUE\":\""+String((int) oeltank_inhalt)+"\",\"UNIT\":\"l\"}");
    Serial.println("Ultrasonic: "+msg);
    mqttClient.publish((ultrasonic_topic+"/inhalt").c_str(), msg.c_str(), true);

    yield();
  }

  //****************************************************
  // Temperature - every 30sec
  //****************************************************
  if (( has_temp_sensor == 1) && (millis() - tempMillisPrev > 30000)) {
    tempMillisPrev = millis();
    temp.requestTemperatures();
   
    //****************************************************
    // Temp ausgeben
    //****************************************************
    String msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR_TYPE\":\"TEMP\", \"OBJECT\":\""+OBJECT+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"Temperature\", \"VALUE\":\""+String(temp.getTempCByIndex(0))+"\",\"UNIT\":\"Grad\"}");
    Serial.println("Temp: "+msg);
    mqttClient.publish(temperature_topic.c_str(), msg.c_str(), true);

    yield();
  }

  //****************************************************
  // DHT11 - every 300sec / 5 min
  //****************************************************
  if (( has_dht11_sensor == 1) && (millis() - dhtMillisPrev > 300000)) {
    dhtMillisPrev = millis();
    // read without samples.
    byte temperature = 0;
    byte humidity = 0;
    int err = SimpleDHTErrSuccess;
    if ((err = dht11.read(pinDHT11, &temperature, &humidity, NULL)) != SimpleDHTErrSuccess) {
      Serial.print("Read DHT11 failed, err="); Serial.println(err);delay(1000);
      return;
    }
  
    Serial.print("Sample OK: ");
    Serial.print((int)temperature); Serial.print(" *C, "); 
    Serial.print((int)humidity); Serial.println(" H");
  
   
    //****************************************************
    // Result ausgeben
    //****************************************************
    String msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR_TYPE\":\"TEMP\", \"OBJECT\":\""+OBJECT+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"Temperature\", \"VALUE\":\""+String(temp.getTempCByIndex(0))+"\",\"UNIT\":\"Grad\"}");
    Serial.println("Temp: "+msg);
    mqttClient.publish(temperature_topic.c_str(), msg.c_str(), true);

    msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR_TYPE\":\"HUMIDITY\", \"OBJECT\":\""+OBJECT+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"Humidity\", \"VALUE\":\""+String(temp.getTempCByIndex(0))+"\",\"UNIT\":\"Grad\"}");
    Serial.println("Temp: "+msg);
    mqttClient.publish(humidity_topic.c_str(), msg.c_str(), true);

    yield();
  }

  // Small tasks - like motion detection

  //****************************************************
  // Tasterstatus abfragen
  //****************************************************
  if (has_taster == 1){
    tasterStatus = digitalRead(pinTaster);
   
    if ( tasterStatus != tasterStatusLast) {
      Serial.print("Button status: "); Serial.println(tasterStatus);
      if ( tasterStatus == 0) {
        //mqttClient.publish(hw_info_topic.c_str(), VERSION.c_str(), true);
        //mqttClient.publish(alarmcontrol_topic.c_str(), String("{\"d\":{\"cmd\":\"toggle\"}}").c_str(), true);
      }
      tasterStatusLast = tasterStatus;
    }
    yield();
  }

  //****************************************************
  // Call home every 120 seconds
  //****************************************************
  if((has_callhome!=0) && (millis() - callhomeMillisPrev > has_callhome * 1000 ) ) {
    callhomeMillisPrev = millis();

    //****************************************************
    // Call home
    //**************************************************** 
    String msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR_TYPE\":\"PRESENCE\", \"OBJECT\":\""+OBJECT+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"SSID\", \"VALUE\":\""+WiFi.SSID()+"\",\"UNIT\":\"N/A\"}");
    Serial.println("Callhome: "+msg);
    mqttClient.publish(callhome_topic.c_str(), msg.c_str(), true);
  }

  //****************************************************
  // PIR motion detection
  //****************************************************
  if (has_motion_sensor == 1){
    pirStatus = digitalRead(pinPIR);
    if (!mqttClient.connected()) {
      mqtt_reconnect();
    }
    mqttClient.loop();

    if (pirStatus == 1 && pirStatusLast != 1) {   // Motion
      // Generate timestamp
      String TS=getTimestamp();

      // Motion: Generate message
      String msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR_TYPE\":\"MOTION\", \"OBJECT\":\""+OBJECT+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"Motion\", \"VALUE\":\"YES\"}");
      Serial.println("Motion: "+msg);
      mqttClient.publish(motion_topic.c_str(), msg.c_str(), true);
    }
    pirStatusLast = pirStatus;
  }

  //****************************************************
  // SW-420 vibration detector
  //****************************************************
  if (has_vibration_sensor == 1){ 
    long measurement =vib_init();
    delay(50);
    
    Serial.println("SW-420:" + String(measurement));
    if (measurement > 1500){
      ledOn(pinLED);
      String msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR_TYPE\":\"VIBRATION\", \"OBJECT\":\""+OBJECT+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"Vibration\", \"VALUE\":\""+measurement+"\"}");
      Serial.println("Vibration: "+msg);
      mqttClient.publish(vibration_topic.c_str(), msg.c_str(), true);
    } else{
      ledOff(pinLED );
    }
  }

  //****************************************************
  // SONOFF Relais  // http://esp8266-server.de/MQTT/Sonoff.html
  //****************************************************
  if (has_sonoff_relais == 1){      
    if (!digitalRead(pinTaster))                          // Wenn Taster betätigt
    {
      delay(50);                                          // Entprellung 
      if (!digitalRead(pinTaster))                        // immer noch betätigt? Noch mal prüfen
      {
        sonoffRelaisState = !sonoffRelaisState;           // Invert
        digitalWrite(pinRelais, sonoffRelaisState);

        String msg=String("{\"TS\":\""+getTimestamp()+"\", \"SENSOR_TYPE\":\"SONOFF-BUTTON\", \"OBJECT\":\""+OBJECT+"\", \"SENSOR\":\""+getESPid()+"\", \"KEY\":\"POW-STATE-CHANGE\", \"VALUE\":\""+sonoffRelaisState+"\"}");
        Serial.println("POW-STATE-CHANGE: "+msg);
        mqttClient.publish(power_topic.c_str(), msg.c_str(), true);

        while (!digitalRead(pinTaster))
          yield();
      }
    }
  }
  
  //****************************************************
  // On the air updates (OTA)
  //****************************************************
  ArduinoOTA.handle();

  //****************************************************
  // ensure that MQTT callback can be executed
  //****************************************************
  yield();
}