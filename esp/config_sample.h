// Object
const char* OBJECT="sample";

// Capabilities
const int has_callhome=0;     // Seconds between call home
const int has_temp_sensor=1;		// DS1820
const int has_motion_sensor=0;
const int has_water_sensor=0;		//
const int has_taster=0;
const int has_ultrasonic_sensor=0;	//
const int has_vibration_sensor=0;	//SW-420
const int has_dht11_sensor=0;		//DHT-11
const int has_sonoff_relais=0;
const int has_led=0;

// WIFI
const char* ssid1     = "SSID1";
const char* password1 = "PWD1";
const char* ssid2     = "SSID2";
const char* password2 = "PWD2";
const char* ssid3     = "SSID3";
const char* password3 = "PWD3";

// MQTT
const char* mqtt_subscribe_topic= "";
const char* mqtt_server   = "mqtt.domain.com";
const char* mqtt_user     = "mqttuser";
const char* mqtt_password = "mqttpassword";
const int   mqtt_port     = 1883;

// PIN definition
const int pinTaster = 16;
const int pinPIR = 12;    // NodeMCU = 2, ESP GPIO 16 = 0
const int pinUltrasonicTrig = 5;  // ESP32: 25; // Triger pin of the HC-SR04
const int pinUltrasonicEcho = 16; // ESP32: 26;  // Echo pin of the HC-SR04  
const int pinVibr=5;      // NodeMCU: D1
const int pinDS18 = 13;   // NodeMCU -> map , ESP GPIO 13 -> no map
const int pinDHT11 = 4;   // NodeMCU: D2
const int pinLED=16;      // NodeMCU: D0
